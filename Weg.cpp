/*
 * Weg.cpp
 *
 *  Created on: 19.11.2023
 *      Author: rehmanchaudhary
 */

#include "Weg.h"
#include "Fahrzeug.h"
#include "Streckenende.h"
#include "Losfahren.h"
#include "Fahrausnahme.h"
#include <exception>
#include "Simulationsobjekt.h"
#include "Verhalten.h"


/**
 * @brief Standardkonstruktor
 */
Weg::Weg()
{
}






/**
 * @brief Konstruktor
 * @param sName : Name des Objekt
 * @param dLaenge : Länge des Wegs
 * @param dTempolimit: Defaultparameter für das Tempolimit
 */
Weg::Weg(const string sName, const double dLaenge, const Tempolimit dTempolimit):Simulationsobjekt(sName), p_dLaenge(dLaenge), p_eTempolimit(dTempolimit)
{}


/**
 * @brief Gibt die Geschwindigkeit zurück (50km/h , 100km/h oder unbegrenzt)
 */
double Weg::getTempolimit() const
{
	return double (p_eTempolimit);
}


/**
 * @brief Gibt den Kopf der Tabelle der Ausgabetabelle aus
 */
void Weg::vKopf()
{
	cout<<resetiosflags(ios::right) << setiosflags(ios::left)<<setw(5)<<left<<"ID"<<setw(15)<<left<<"|Name"<<setw(25)<<setw(20)<<right<<"|Laenge"<<setw(18)<<right<<"|Fahrzeug"<<endl;
	cout<<"-------------------------------------------------------------------------------------------------------------"<<endl;
}

/**
 * @brief Simuliert den Weg
 */
void Weg::vSimulieren()
	{

		p_pFahrzeuge.vAktualisieren();

		//list< unique_ptr< Fahrzeug > >::iterator it;

		//simulieren aller Fahrzeuge anhand einer range-basierter Schleife
		for (auto &fahrzeug :  p_pFahrzeuge)
		{
/*Innerhalb des try-Blocks wird nach spezifischen Ausnahmen vom Typ Fahrausnahme gesucht, die von den Methoden vSimulieren() oder vZeichnen()
 geworfen werden könnten. Falls eine solche Ausnahme auftritt, wird der entsprechende catch-Block ausgeführt.*/
			try
			{
	 			(fahrzeug)->vSimulieren(); 	//simulieren aller Fahrzeuge.
				(fahrzeug)->vZeichnen(*this); //Zeichnen des Fahrzeuges im Weg
			}
/*Im catch-Block wird die Methode vBearbeiten() der aufgetretenen Ausnahme (Fahrausnahme) aufgerufen, um sie zu behandeln*/
			catch (Fahrausnahme& Fahrzeug)
			{
				Fahrzeug.vBearbeiten(); //wenn eine bestimmte Ausnahme (entweder Losfahren oder Streckenende) geworfen wurde,
								 //dann wird sie hier gefangen und ihre entsprechende vBearbeiten() wird dann aufgereufen.
			}


		}
		p_pFahrzeuge.vAktualisieren();

	}

/**
 * @brief Annahme Funktion, welche die Instanzen der Fahrzeuge hinten in der Liste anheftet
 */
void Weg::vAnnahme(std::unique_ptr<Fahrzeug> aFzg)
{
	if( aFzg != nullptr )
	{
	aFzg -> vNeueStrecke(*this);
	p_pFahrzeuge.push_back(std::move(aFzg));
	}
}

/**
 * @brief Annahme Funktion, welche die Instanzen der Fahrzeuge hinten in der Liste anheftet
 */
void Weg::vAnnahme(std::unique_ptr<Fahrzeug> aFzg, double StartZeit )			//für die parkenden Fahrzeuge
{
	if( aFzg != nullptr )
	{
	aFzg->vNeueStrecke(*this,StartZeit);
	p_pFahrzeuge.push_front(std::move(aFzg));
	}
}

/**
 * @return gibt die in den  Membervariaben p_dLaenge  gespeicherten Wert zurück
 */
double Weg::getLaenge() const
{
	return p_dLaenge;
}

/**
 * @brief Abnahme Funktion
 */
std::unique_ptr<Fahrzeug> Weg::pAbgabe(const Fahrzeug& fahrzeug)
{
	// parkende Fahrzeug f in der Liste "p_pFahrzeuge" suchen
	for (auto it = p_pFahrzeuge.begin(); it != p_pFahrzeuge.end(); it++)
	{
		// falls gefunden
		if (*it == nullptr)
		{
			continue;
		}
		if ((*it).get()->operator==(fahrzeug))
		{
			// gefundenes Fahrzeug in "pGefunden" speichern
			unique_ptr<Fahrzeug> pGefunden = std::move((*it)); // lokale Variable
			p_pFahrzeuge.erase(it);	// loeschen das gefundene Fahrzeug aus der List (it)
			return std::move(pGefunden); // zurueckgeben
		}

	}

	return nullptr;	// Sonderfall：das uebergebene Fahrzeug nicht gefunden wird
}


/*
* //-----------getter fuer die Liste der Fahrzeuge auf dem Weg--------------
std::list<std::unique_ptr<Fahrzeug>> Weg::pGetList()
{
	return p_pFahrzeuge;
}
*/





/**
 * @brief Ausgabefunktion in der Klasse Weg
 */
ostream& Weg::oAusgeben(ostream& out) const
	{

	Simulationsobjekt::oAusgeben(out);
	out<< "        : "<<setw(11)<<right<< p_dLaenge;
	out<<std::resetiosflags(std::ios::adjustfield);
	out<<setiosflags(std::ios::left);
	out<<"    (";
	for (const auto&fahrzeug :p_pFahrzeuge)
	{
		out<< fahrzeug->Fahrzeug::getName()<<" ,";
	}
	out<<")";


	return out;
	}

/**
 * @brief Destruktor: löscht die erstellten Instanzen
 */
Weg::~Weg()
{}

ostream& operator <<(ostream& out, const Weg & Weg)
{
	Weg.oAusgeben(out);
	return out;
}


