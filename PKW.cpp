/*
 * PKW.cpp
 *
 *  Created on: 02.11.2023
 *      Author: rehmanchaudhary
 */

#include "PKW.h"
#include "Verhalten.h"
#include "Weg.h"
#include "SimuClient.h"




/**
 * @brief Standardkonstruktor
 */
PKW::PKW():Fahrzeug(),
p_dVerbrauch(100),p_dTankvolumen(55), p_dTankinhalt(55/2)
{}



/**
 * @brief Konstruktor
 * nutzt den Konstruktor der Basisklasse zum Erstellen einer Instanz
 * @param sName: Name des PKWs
 * @param dMaxGeschwindigkeit: Angabe der maximalen Geschwidnigkeit des PKWs
 * @param dVerbrauch: gibt den Verbrach in Liter/100km an
 * @param Tankvolumen: Gibt das Tankvolumen an-->hier als Default Wert mit 55 Litern
 */
PKW::PKW(const string sName, const double dMaxGeschwindigkeit): Fahrzeug(sName,dMaxGeschwindigkeit),			//Konstruktor aus der Basisklasse wird aufgerufen und übergibt ID,Name und max. Geschwindigkeit
		p_dVerbrauch(100),p_dTankvolumen(55), p_dTankinhalt(55/2)
{}




/**
 * @briefKonstruktor
 * nutzt den Konstruktor der Basisklasse zum Erstellen einer Instanz
 * @param sName: Name des PKWs
 * @param dMaxGeschwindigkeit: Angabe der maximalen Geschwidnigkeit des PKWs
 * @param dVerbrauch: gibt den Verbrach in Liter/100km an
 * @param Tankvolumen: Gibt das Tankvolumen an-->hier als Default Wert mit 55 Litern
 */
PKW::PKW(const string sName, const double dMaxGeschwindigkeit, const double dVerbrauch): Fahrzeug(sName,dMaxGeschwindigkeit),			//Konstruktor aus der Basisklasse wird aufgerufen und übergibt ID,Name und max. Geschwindigkeit
		p_dVerbrauch(100),p_dTankvolumen(55), p_dTankinhalt(55/2)
{}



/**
 * @briefKonstruktor
 * nutzt den Konstruktor der Basisklasse zum Erstellen einer Instanz
 * @param sName: Name des PKWs
 * @param dMaxGeschwindigkeit: Angabe der maximalen Geschwidnigkeit des PKWs
 * @param dVerbrauch: gibt den Verbrach in Liter/100km an
 * @param Tankvolumen: Gibt das Tankvolumen an-->hier als Default Wert mit 55 Litern
 */
PKW::PKW(const string sName, const double dMaxGeschwindigkeit, const double dVerbrauch, const double dTankvolumen = 55 ):
		Fahrzeug(sName,dMaxGeschwindigkeit),p_dVerbrauch(dVerbrauch),p_dTankvolumen(dTankvolumen), p_dTankinhalt(dTankvolumen/2)
{}



/**
 * @brief Destruktor
 */
PKW::~PKW()
{}




/**
 *@brief Funktion, die uns nachträglichen  Betanken der PKWs
 *@param Menge: gibt die Menge zum nachträglichen Betanken des PKWs an
 */
double PKW::dTanken(double dMenge)
{
		double dMaxTankbareMenge = p_dTankvolumen - p_dTankinhalt;

		if(dMenge < dMaxTankbareMenge)												//falls Menge kleiner ist als die maximal tankabre Menge --> dann tanken wir die noch passende Menge
		{
			p_dTankinhalt += dMenge;
			return dMenge;
		}
		else																		//ansonsten tanken wir die maximale Menge, die noch zu tanken ist
		{
			p_dTankinhalt += dMaxTankbareMenge;
					return p_dTankinhalt;
		}
}

/**
 * @brief: Gibt den aktuellen Zustand des Fahrzeugs aus
 * mit Hilfe von dGlobaleZeit wird ermittet, wieviel Zeit seit dem letzten Simulationsschritt vergangen ist
 * mit dieser Information wird dann der Zustand des Fahrzeugs aktualisiert
 */
void PKW::vSimulieren()
{
	if(p_dTankinhalt >0)
	{
		double pInitilasatorFuerDieGefahreneStrecke;
		pInitilasatorFuerDieGefahreneStrecke = p_dGesamtStrecke;
		Fahrzeug::vSimulieren();

		double dGefahreneStrecke =   p_dGesamtStrecke - pInitilasatorFuerDieGefahreneStrecke;

		p_dTankinhalt = p_dTankinhalt - dGefahreneStrecke * p_dVerbrauch /100;


	}

	else
	{
		p_dZeit = dGlobaleZeit;
		p_dTankinhalt =0;
	}
}

double PKW::dTankVerbrauch() const
{
	return p_dGesamtStrecke * p_dVerbrauch/100;
}


/**
 * @brief Erstellt den Kopf der Tabelle
 */
void PKW::vKopf()
{
	Fahrzeug::vKopf();

}


/**
 * @brief Erstellt die ausgegebene Tabelle
 */
void PKW::vAusgeben() const
{
	Fahrzeug::vAusgeben();
	cout<<resetiosflags(ios::fixed)<<setw(20)<<setprecision(2)<<fixed<<right<< dTankVerbrauch() <<setw(30)<<setprecision(2)<<fixed<<right<<p_dTankinhalt;
}

double PKW::dGeschwindigkeit() const
{

	Weg& Weg = p_pVerhalten -> getWeg();        /* auf dem Weg wird durch das Verhalten-Objekt des PKWs zugegriffen */
	double limit = Weg.getTempolimit();            /* Tempoliit wird hier gespeichert */

	if( limit > p_dMaxGeschwindigkeit )
	{

		return p_dMaxGeschwindigkeit;
	}

	return limit;                              /* wenn die Maximale Geschwindigkeit grösser ist, wird doch das limit zurückgegeben */
}

/**
 *@brief Überladene Ausgeben-Funktion
 *@return out : gibt die gewünschte Ausgabe
*/
ostream & PKW::oAusgeben(ostream & out) const
{
	Fahrzeug::vAusgeben();
	cout<<resetiosflags(ios::fixed)<<setw(20)<<setprecision(2)<<fixed<<right<< dTankVerbrauch() <<setw(30)<<setprecision(2)<<fixed<<right<<p_dTankinhalt;
	return out;
}

/**
 * @brief Zeichnet beim SimuClient die Strecke vom PKW
 * @param Weg ist eine Referenz auf dem Weg
 * @param RelPosition gibt die relative Position des Fahrobjekts zur Gesamtstrecke an
 * @param WegName gibt den Names des Weges an
 * @param PKWName gibt den Names des PKWs zurück
 * @param KmH gibt die Geschwindigkeit des PKW in Kmh an
 */
void PKW::vZeichnen(const Weg& 	Weg) const
{
	double RelPosition = this->dgetAbschnittStrecke() / Weg.getLaenge(); //realtive Position des PKWs zur Laenge des Weges:
																 //Wert zwichen 0 & 1.
	string WegName = Weg.getName(); 			//Name des Weges.
	string PKWName = this->getName();   //Name des PKWs.
	double KmH = this->dGeschwindigkeit(); //Geschwindigkeit des PKWs in KmH.
	double Tank = this->p_dTankinhalt; //Tankinhalt des PKWs.

	bZeichnePKW(PKWName, WegName, RelPosition, KmH, Tank);


}



void PKW::operator=(const PKW & fahrzeug)
{
	Fahrzeug::operator=(fahrzeug);
	p_dTankvolumen = fahrzeug.p_dTankvolumen;
	p_dTankinhalt = p_dTankvolumen / 2;
}


