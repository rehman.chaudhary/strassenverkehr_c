/*
 * Verhalten.h
 *
 *  Created on: 29.11.2023
 *      Author: rehmanchaudhary
 */
#pragma once
#ifndef SRC_VERHALTEN_H_
#define SRC_VERHALTEN_H_
//wir inkludieren nicht die anderen HeaderFiles um Rekursion zu vermeiden


class Weg;
class Fahrzeug;


class Verhalten
{
public:
	Verhalten();
	Verhalten(Weg& Weg);
	virtual double  dStrecke(Fahrzeug& aFzg, double dZeitIntervall)=0; // rein virtuell
	virtual ~Verhalten();
	double getWegLaenge() const;
	Weg& getWeg();

protected: Weg &p_pWeg;

private:


};

#endif /* SRC_VERHALTEN_H_ */
