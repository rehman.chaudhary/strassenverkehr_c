/*
 * Fahrrad.cpp
 *
 *  Created on: 02.11.2023
 *      Author: rehmanchaudhary
 */

#include "Fahrrad.h"
#include "Weg.h"
#include "SimuClient.h"
#include <math.h>


/**
 * @brief Standardkonstruktor
 */
Fahrrad::Fahrrad():Fahrzeug()
{}



/**
 * @brief Konstruktor, der parameter und tyspezifischen Konstruktor aus der Basisklasse Fahrzeug mit allen Werten erstellt
 * @param sName: Name des Autos
 * @param dMaxGeschwindigkeit: gibt die maximale Geschwindigkeit an
 */
Fahrrad::Fahrrad(string sName,double dMaxGeschwindigkeit):Fahrzeug(sName,dMaxGeschwindigkeit)
{
}



/**
 * @brief Destruktor: löscht die erstellten Instanzen
 */
Fahrrad::~Fahrrad()
{}







/**
 * @brief Passt die Geschwindigkeit an
 * verringert alle 20 KM die Geschwindigkeit um 10%
 * @return gibt die maximale Geschwindigkeit zurück
 */
double Fahrrad::dGeschwindigkeit() const

{
	if(p_dGesamtStrecke <20)
	{
	return p_dMaxGeschwindigkeit;
	}
	else
	{
		double dGeschwindigkeit = pow(0.9, (int) p_dGesamtStrecke/20) * p_dMaxGeschwindigkeit;
		if(dGeschwindigkeit >= 12 )
		{
			return dGeschwindigkeit;
		}
		else
		{
			return 12;
		}
	}
}


/**
 * @brief Zeichnet beim SimuClient die Strecke vom Fahrrad
 * Weg ist eine Referenz auf dem Weg
 * @param RelPosition gibt die relative Position des Fahrobjekts zur Gesamtstrecke an
 * @param WegName gibt den Names des Weges an
 * @param FahrradName gibt den Names des Fahrrads zurück
 * @param KmH gibt die Geschwindigkeit des Fahrrads in Kmh an
 */
void Fahrrad::vZeichnen(const Weg& Weg) const

{
	double RelPosition = this->dgetAbschnittStrecke() / Weg.getLaenge(); //realtive Position des PKWs zur Laenge des Weges:
																 //Wert zwichen 0 & 1.
	string WegName = Weg.getName(); 			//Name des Weges.
	string FahrradName = this->getName(); //Name des Fahrrads.
	double KmH = this->dGeschwindigkeit(); //Geschwindigkeit des Fahrrads in KmH.


	bZeichneFahrrad(FahrradName, WegName, RelPosition, KmH);

}


