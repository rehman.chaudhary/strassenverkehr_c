/*
 * Weg.h
 *
 *  Created on: 19.11.2023
 *      Author: rehmanchaudhary
 */

#ifndef SRC_WEG_H_
#define SRC_WEG_H_
#include "Simulationsobjekt.h"
#include "vertagt_liste.h"
#include <list>


using namespace vertagt;
enum class Tempolimit {
						Innerorts = 50,
						Landstrasse = 100,
						Autobahn = numeric_limits<int>::max()
					  };


class Fahrzeug;

class Weg : public Simulationsobjekt
{
public:
	Weg();
	Weg(const string sName, const double dLaenge, const Tempolimit dTempolimit = Tempolimit::Autobahn);
	double getTempolimit() const;
	void static vKopf();
	virtual ostream& oAusgeben(ostream& out) const;
	virtual void  vSimulieren();
	void vAnnahme(unique_ptr<Fahrzeug>);
	void vAnnahme(unique_ptr<Fahrzeug>, double);
	double getLaenge() const;
	unique_ptr<Fahrzeug> pAbgabe(const Fahrzeug& fahrzeug);
	//std::list<std::unique_ptr<Fahrzeug>> pGetList();
	VListe< unique_ptr< Fahrzeug > > p_pFahrzeuge;


	virtual ~Weg();
protected:
	double p_dLaenge=0;
	//std::list<unique_ptr<Fahrzeug>> p_pFahrzeuge;				//ersetzen druchb VListe

private:
	const Tempolimit p_eTempolimit = Tempolimit::Autobahn;




};

#endif /* SRC_WEG_H_ */

ostream& operator <<(ostream& out, const Weg& Fahrzeug);

