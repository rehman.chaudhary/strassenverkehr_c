/*
 * Simulationsobjekt.h
 *
 *  Created on: 19.11.2023
 *      Author: rehmanchaudhary
 */

#pragma once
#include <string>
#include <iostream>
#include <iomanip>
#include <memory>
#include <vector>
#include <limits>
#include <cmath>
#include <list>
#include "SimuClient.h"

using namespace std;

#ifndef SRC_SIMULATIONSOBJEKT_H_
#define SRC_SIMULATIONSOBJEKT_H_

class Simulationsobjekt {
public:
	Simulationsobjekt();
	Simulationsobjekt(const string sName);
	static void vKopf();
	void virtual  vAusgeben()const;
	virtual ostream& oAusgeben(ostream& out) const;
	string getName() const;
	virtual void vSimulieren() = 0;





	virtual ~Simulationsobjekt();

protected:
	string p_sName;
	double p_dZeit = 0.0;
	static int p_iMaxID;
	int  p_iID ;


	double p_pVerhalten;

private:

									//löschen des Copy Konstruktors
};


#endif /* SRC_SIMULATIONSOBJEKT_H_ */
ostream& operator <<(ostream & out,  const Simulationsobjekt&  Simulationsobjekt);
