/*
 * PKW.h
 *
 *  Created on: 02.11.2023
 *      Author: rehmanchaudhary
 */
#pragma once
#ifndef PKW_H_
#define PKW_H_
#include "Fahrzeug.h"


class Weg;
class Verhalten;

class PKW : public Fahrzeug
{
public:
	PKW();
	PKW(const string sName, const double dMaxGeschwindigkeit);
	PKW(const string sName, const double dMaxGeschwindigkeit, const double dVerbrauch);
	PKW(const string sName,const  double dMaxGeschwindigkeit, const  double  dVerbrauch, const  double dTankvolumen);
	virtual ~PKW();
	virtual double dTanken(double dMenge = numeric_limits<double>::infinity());			//falls wir keinen Wert übergeben wollen, setzen wir den Wert auf den grösstmöglichen Wert(also es wird defintiv vollgetankt)
	virtual void vSimulieren();
	void static vKopf();
	void virtual vAusgeben() const ;
	double  dTankVerbrauch() const;
	double dGeschwindigkeit()const ;
	void operator = (const PKW& fahrzeug);
	virtual void vZeichnen(const Weg& Weg) const override ;
	 ostream & oAusgeben(ostream & out) const;



private:
	double p_dTankvolumen;
	double p_dVerbrauch;
	double p_dTankinhalt;

};

#endif /* PKW_H_ */
