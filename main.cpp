//============================================================================
// Name        : Aufgabenblock_1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Fahrzeug.h"
#include "PKW.h"
#include "Fahrrad.h"
#include <vector>
#include<math.h>
#include "Simulationsobjekt.h"
#include "Weg.h"
#include "SimuClient.h"
#include <random>
#include "vertagt_liste.h"

double dGlobaleZeit = 0;		//Bekanntmachen dieser Variablen
double dZeitTakt = 0.1;
#define ABWEICHUNG  0.000000001

using namespace std;
using namespace vertagt;


void vFunktion1_0_Zusatz()
{
	//Zusatz: Eingabe um Fahrzeug zu erstellen
	string sEingabe;
	cout<<"Bitte geben sie den Namen des zu erstellenden Fahrzeugs ein: "<<endl;
	cin>>  sEingabe;
	Fahrzeug *p_F0= new Fahrzeug(sEingabe);
	delete p_F0;
}




 void vFunktion1_3()
{
	Fahrzeug F1;																						//statisch erzeugte Instanzen der Klasse Fahrzeug
	Fahrzeug F2("Audi");
	Fahrzeug F3("Mercedes Benz");


	Fahrzeug *p_F4= new Fahrzeug("BMW");																//dynamisch erzeugetes Objekt der Klasse Fahezeug
	Fahrzeug *p_F5= new Fahrzeug;

																										//erstellen eines Fahrzeugs mit negativer Geschwindigkeit, sodass der Default Wert auf 0 gesetzt wir
	delete p_F4;																					//löschen der dynamisch erzeugten Objekte der Klasse Fahrzeug
	delete p_F5;
}

 void vFunktion1_4()
 {
	 	unique_ptr<Fahrzeug> F1 = make_unique<Fahrzeug>("Tesla");										//erstellen der Instanzen mit unique Pointer
	 	unique_ptr<Fahrzeug> F2 = make_unique<Fahrzeug>();




	 	shared_ptr<Fahrzeug> F3 = make_shared<Fahrzeug>("Aston Martin");								//Instanz der Klasse Fahrzeug erstellt mit shaared Pointer
	 	shared_ptr<Fahrzeug> F4= make_shared<Fahrzeug>();
	 	cout<<"Referenzanzahl des Pointers nach der ersten Zuweisung: "<<F3.use_count()<<endl;			//1

	 	shared_ptr<Fahrzeug> F5 =   F3;																	//Übergabe nur bei SharedPointer erlaubt
	 	shared_ptr<Fahrzeug> F6 =   F3;
	 	shared_ptr<Fahrzeug> F7 =   F4;
	 	cout<<"Referenzanzahl des Pointers nach der dritten Zuweisung: "<<F3.use_count()<<endl;			//jetzt zeigen 3 Referenzen auf die Ressource F3


	 	vector<unique_ptr<Fahrzeug>> Vektor_Unique;														//in diesem Vektor können nur UniquePointer eingelesen werden
	 	Vektor_Unique.push_back(move(F1));																//Besitzwechsel mit dem MOVE()-Befehl
	 	Vektor_Unique.push_back(move(F2));
	 	cout<<"Länge des Unique_Vektors: "<<Vektor_Unique.size()<<endl;									//Länge des Vektors
	 	Vektor_Unique.clear();																			//Löschen des Vektors
	 	cout<<"Länge des Unique_Vektors nun nachdem Löschen: "<<Vektor_Unique.size()<<endl;



	 	vector<shared_ptr<Fahrzeug>> Vektor_Shared;
	 	Vektor_Shared.push_back(move(F3));																//in diesem Vektor können nur Shared Pointer eingelesen werden
	 	Vektor_Shared.push_back(F4);
	 	cout<<"Länge des Shared_Vektors: "<<Vektor_Shared.size()<<endl;									//Länge des Vektors

	 	cout<<"Anzhal der Referenzen von der Instanz F3 (mit Move()-Befehl) : "<<F3.use_count()<<endl;
	 	cout<<"Anzhal der Referenzen von der Instanz F4 (ohne Move()-Befehl): "<<F4.use_count()<<endl;	//Gibt die Anzhal der Referenzen zurück --> hier 3 da wir 2 Instanzen von F8 haben und noch eins beim Verschieben in dem Vektor. Funktioniert NICHT bei move()-Befehl

	 	Vektor_Shared.clear();
	 	cout<<"Länge des Shared_Vektors nun nachdem Löschen: "<<Vektor_Shared.size()<<endl;


 }


void vFunktion1_5()
{
	Fahrzeug F1("Tesla", 200);
	Fahrzeug F2("Porsche", -100);
}

void vFunktion1_6()
{
	Fahrzeug::vKopf();
	Fahrzeug F1("Mercedes Benz");
	Fahrzeug F2("Audi",200);
	F1.vAusgeben();
	cout<<endl;									//manueller Zeilensprung, den wir im Hauptprogramm schreiben sollten


	F2.vAusgeben();
	cout<<endl;																							//manueller Zeilensprung, den wir im Hauptprogramm schreiben sollten


}
void vFunktion_1a()
{

	string sEingabe1;
	cout<<"Bitte geben sie den Namen des ersten Fahrzeugs ein: "<<endl;
	cin>> sEingabe1;

	double Geschwindigkeit_F1;
	cout<<"Bitte geben sie die maximale Geschwidnigkeit des ersten Fahrzeugs ein: "<<endl;
	cin>>Geschwindigkeit_F1;


	string sEingabe2;
	cout<<"Bitte geben sie den Namen des zweiten Fahrzeugs ein: "<<endl;
	cin>>sEingabe2;

	double Geschwindigkeit_F2;
	cout<<"Bitte geben sie die maximale Geschwidnigkeit des zweiten Fahrzeugs ein: "<<endl;
	cin>>Geschwindigkeit_F2;

	string sEingabe3;
	cout<<"Bitte geben sie den Namen des dritten Fahrzeugs ein: "<<endl;
	cin>>sEingabe3;

	double Geschwindigkeit_F3;
	cout<<"Bitte geben sie die maximale Geschwidnigkeit des dritten Fahrzeugs ein: "<<endl;
	cin>>Geschwindigkeit_F3;


	unique_ptr<Fahrzeug> F1 = make_unique<Fahrzeug>(sEingabe1, Geschwindigkeit_F1);
	unique_ptr<Fahrzeug> F2 = make_unique<Fahrzeug>(sEingabe2, Geschwindigkeit_F2);
	unique_ptr<Fahrzeug> F3 = make_unique<Fahrzeug>(sEingabe3, Geschwindigkeit_F3);

	vector<unique_ptr<Fahrzeug>> VectorV1;
	VectorV1.push_back(move(F1));
	VectorV1.push_back(move(F2));
	VectorV1.push_back(move(F3));

	for (extern double dGlobaleZeit; dGlobaleZeit <= 10; dGlobaleZeit += dZeitTakt)
		{
		Fahrzeug::vKopf();
			for (int i = 0; i <= 2; i++)
			{
				VectorV1[i]->vSimulieren();
			}

			for (int k = 0; k <= 2; k++)
			{
				VectorV1[k]->vAusgeben();
				cout << endl;
			}

			cout << endl;
		}


}
void vAufgabe_2()
{

	unique_ptr<PKW> P1 = make_unique<PKW> ("Audi",100,10,50);
	unique_ptr<PKW> P2 = make_unique<PKW> ("Benz",120,10,50);
	unique_ptr<Fahrrad> P3 = make_unique<Fahrrad> ("Rixon",100);
	vector<unique_ptr<Fahrzeug>> VectorPKW;														//für Simulation von PKW --> Vektor muss von KLASSE PKW sein
	VectorPKW.push_back(move(P1));
	VectorPKW.push_back(move(P2));
	VectorPKW.push_back(move(P3));



	for (extern double dGlobaleZeit; dGlobaleZeit <= 10; dGlobaleZeit += dZeitTakt)
	{
				Fahrzeug::vKopf();

							for (int i = 0; i <= 2; i++)
									{
											VectorPKW[i]->vSimulieren();
											VectorPKW[i]->vAusgeben();cout<<endl;


									}



								if(fabs(dGlobaleZeit-3)< ABWEICHUNG)
										{
											for (int i = 0; i <= 1; i++)
													{
														VectorPKW[i]->dTanken();
													}
										}
							cout<<endl;
			}

}







void vAufgabe_3()
{

		PKW a("PKW1", 50, 5, 0);
		Fahrrad b("Fahrrad", 20);
		PKW c("PKW2", 60, 5, 55);

		Fahrzeug::vKopf();



		cout << a << endl;
		cout << b << endl;
		cout << c << endl;
		//cout << "Das ist ein Test" << endl;

		system("pause");

		for (int i = 0; i < 4; i++)
		{
			extern double dGlobaleZeit;
			a.vSimulieren();
			b.vSimulieren();
			c.vSimulieren();
			dGlobaleZeit += 1;
		}
		cout << "Die Fahrzeuge wurden simuliert. " << endl << endl;
		cout << a << endl;
		cout << b << endl;
		cout << c << endl;



		if (a < c)
		{
			cout << "PKW2 ist eine weitere Distanz gefahren als PKW1" << endl;
		}
		else
		{
			cout << "PKW1 ist eine weitere Distanz gefahren als PKW2" << endl;
		}

		if (b < c)
		{
			cout << "PKW2 ist eine weitere Distanz gefahren als Fahrrad1" << endl;
		}
		else
		{
			cout << "Fahrrad1 ist eine weitere Distanz gefahren als PKW2" << endl;
		}

		system("pause");

		Fahrzeug::vKopf();

		cout << a << endl;
		cout << b << endl;
		cout << c << endl;

		PKW d;
		d = a;

		Fahrrad e;
		e = b;

		Fahrzeug::vKopf();

		cout << d << endl;
		cout << e << endl;
	}


void vAufgabe_AB1() {

    int l = 0; // Laufindex für gezielte AUsgabe
    vector<int> ausgabe{15};
    double dTakt = 0.3;

    std::vector<unique_ptr<Fahrzeug>> vecFahrzeuge;
    vecFahrzeuge.push_back(make_unique <PKW>("Audi", 217, 10.7));
    vecFahrzeuge.push_back(make_unique <Fahrrad>("BMX", 21.4));
    for (dGlobaleZeit = 0; dGlobaleZeit < 6; dGlobaleZeit += dTakt)
    {
        auto itL = find(ausgabe.begin(), ausgabe.end(), l);
        if (itL != ausgabe.end()) {
            std::cout << std::endl << l <<  " Globalezeit = " << dGlobaleZeit << std::endl;
            Fahrzeug::vKopf();
        }

        for (int i = 0; i < vecFahrzeuge.size(); i++)
        {
            vecFahrzeuge[i]->vSimulieren();
            if (fabs(dGlobaleZeit - 3.0) < dTakt/2)
            {
                vecFahrzeuge[i]->dTanken();
            }
            if (itL != ausgabe.end()) {
                std::cout << *vecFahrzeuge[i] << endl;
            }
        }
        l++;
    }
    char c;
    std::cin >> c;
}

void vAufgabe_4()
{
	Weg A("Kapitelstrasse 6", 10);
	Weg B("Engelsstrasse 1", 10);


	Weg::vKopf();
	cout << A <<endl;
	cout << B <<endl;


}


void vAufgabe_5()
{
	Weg W("Engelsstrasse 1",300, Tempolimit::Autobahn);
	unique_ptr<Fahrzeug> F1 =make_unique<Fahrzeug>("BMW",150);
	unique_ptr<Fahrzeug> F2 =make_unique<Fahrzeug>("Benz",200);
	unique_ptr<Fahrzeug> F3 =make_unique<Fahrzeug>("Audi",100);

	F1->vNeueStrecke(W);

	F2->vNeueStrecke(W);

	F3->vNeueStrecke(W);

	W.vAnnahme(move(F1));
	W.vAnnahme(move(F2));
	W.vAnnahme(move(F3),3.0);							//für parkende Autos -->werden mit push_front oben auf die Liste gesetzt




	 for ( ; dGlobaleZeit < 10; dGlobaleZeit+= 1.0)
	 {		cout << endl;
		    cout << endl;
		 	W.vSimulieren();
	        W.vKopf();
	        cout << W << endl;

	 }
}

void vAufgabe_6()

{
	bInitialisiereGrafik(800,500);
	Weg A("Strasse_1", 500, Tempolimit::Autobahn);
	Weg B("Strasse_2", 500);



	int WegAB[4] = { 100, 250, 600, 250 };

	bZeichneStrasse(A.getName(), B.getName(), A.getLaenge(), 2, WegAB);
	//bZeichnePKW("Benz", "Strasse_1", 0.5, 60.0, 70.0);



	/*
    unique_ptr<Fahrzeug>X= make_unique<PKW>("Audi", 200,10,100);
	unique_ptr<Fahrzeug>Y= make_unique<PKW>("Benz", 300,10,100);
	unique_ptr<Fahrzeug>P=make_unique<PKW>("BMW", 250,10,100);
	unique_ptr<Fahrzeug>Z= make_unique<Fahrrad>("BMX", 50);


	A.vAnnahme(move(X));
	A.vAnnahme(move(Y));
	A.vAnnahme(move(P),3.0);				//parkendes Fahrzeug
	A.vAnnahme(move(Z));
	*/


	A.vAnnahme( std::make_unique< PKW >( "BMW", 250, 10, 50 ) );
	A.vAnnahme( std::make_unique< PKW >( "Benz", 200, 10, 50 ),3.0 );
	A.vAnnahme( std::make_unique< PKW >( "Audi", 100, 10, 50 ) );

	A.vAnnahme(std::make_unique<Fahrrad>("BMX",50));


	cout << endl;
	 for ( ; dGlobaleZeit < 10; dGlobaleZeit+= 1)
	 {
		 vSetzeZeit(dGlobaleZeit);
		 cout<<endl;
		 A.vSimulieren();

		 A.vKopf();
		 vSleep(300);
		 cout << A << endl;
	 }


}








void vListeAusgeben( vertagt::VListe< int >& liste ) { /* Liste ausgeben */

	list< int >::iterator it;
	cout << "Die Liste enthält folgende Elemente: " << endl;

	for( it = liste.begin(); it != liste.end(); it++ ) {

		cout << *it << " ";
	}

	cout << endl;
}


void vAufgabe_6a()
{
 VListe< int > Randomvaluelist;			//erstellen einer VListe
 int seed  = 0,  a = 1, b = 10;

	static mt19937 device( seed );					//  Es wird ein Mersenne-Twister-Zufallszahlengenerator mit der angegebenen seed initialisiert. Der Mersenne-Twister ist ein weit verbreiteter Zufallszahlengenerator.
	uniform_int_distribution< int > dist( a,b );    //Es wird eine Verteilung definiert, die gleichmäßig verteilte Ganzzahlen zwischen a und b erzeugt. Diese Verteilung wird verwendet, um Zufallszahlen im definierten Bereich zu generieren.

	for( int i = 0; i < 10; i++ )
	{
		int random_number = dist( device );					//erzeugen einer Zufallszahl mit der dist Funktion..und dem ZUfallsgenerator device
		Randomvaluelist.push_back( random_number );

		Randomvaluelist.vAktualisieren();
	}

 Randomvaluelist.vAktualisieren();

 list< int >::iterator it;
 	for( auto it = Randomvaluelist.begin(); it != Randomvaluelist.end(); it++ )
 	{

 		if( *it > 5 )
 		{

 			Randomvaluelist.erase( it ); /* Hier wird dann die Liste gelöscht */

 		}

 	}
 	vListeAusgeben( Randomvaluelist );


 	Randomvaluelist.vAktualisieren();

	vListeAusgeben( Randomvaluelist );


	Randomvaluelist.push_front( 1 ); /* fügt Zahl vor dem ersten Objekt ein */
	Randomvaluelist.push_back( 2); /* fügt Zahl am Ende ein */
	Randomvaluelist.vAktualisieren();

	vListeAusgeben( Randomvaluelist );

}


int main()
	{
		vAufgabe_6();
		return 0;
	}
