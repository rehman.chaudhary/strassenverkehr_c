/*
 * Fahrzeug.h
 *
 *  Created on: 10.10.2023
 *      Author: rehmanchaudhary
 */

#include "Simulationsobjekt.h"


using namespace std;




#ifndef FAHRZEUG_H_
#define FAHRZEUG_H_


extern double dGlobaleZeit;

class Weg;
class Verhalten;


class Fahrzeug : public Simulationsobjekt
{
public:
	Fahrzeug();																	//StandardKonstruktor
	Fahrzeug(const string sName);												//Konstruktor mit String-Übergabeparameter
	Fahrzeug(const string sName, const double dMaxGeschwindigkeit);				//Komnstruktor mit String Übergabe für den Namen und int für die max. Geschwindigkeit
	virtual ~Fahrzeug();														//Destruktor

	void static vKopf();
	void   virtual vAusgeben() const;
	virtual void vSimulieren();
	virtual double dGeschwindigkeit() const;
	double dTankVerbrauch() const;
	virtual double dTanken(double dMenge = numeric_limits<double>::infinity());
	virtual ostream &oAusgeben(ostream& out) const ;
	bool operator < (const Fahrzeug& fahrzeug) const;
	void operator=(const Fahrzeug & fahrzeug);
	bool operator ==(const Fahrzeug & fahrzeug) const ;
	double getWegLaenge()const;
	double dgetAbschnittStrecke() const;
	virtual void vNeueStrecke(Weg &Weg);
	double getZeit() const;
	virtual void vNeueStrecke(Weg& weg,double dStartzeit);
	virtual void vZeichnen(const Weg& Weg) const;
	string getName() const;


protected:
		string p_sName;
	    double p_dMaxGeschwindigkeit;
		double p_dGesamtStrecke=0;
		double p_dGesamtZeit = 0.0;														//zur Sychronisation der Zeit
		double p_dGlobaleZeit=0;
		double p_dAbschnittStrecke;
		shared_ptr<Verhalten> p_pVerhalten;




private:
	int  p_iID ;																	//individuelle 	ID-Kennung															//erzeugen eines statischen Klassenelements
	Fahrzeug(const Fahrzeug&) = delete;												//löschen des Copykonstruktors
	//std::unique_ptr<Verhalten> p_pVerhalten = nullptr;


};

#endif /* FAHRZEUG_H_ */
