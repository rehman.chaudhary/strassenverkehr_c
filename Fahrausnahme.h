/*
 * Fahrausnahme.h
 *
 *  Created on: 02.12.2023
 *      Author: rehmanchaudhary
 */

#ifndef SRC_FAHRAUSNAHME_H_
#define SRC_FAHRAUSNAHME_H_
#pragma once
#include <exception>
#include <stdexcept>
#include "Weg.h"
#include "Fahrzeug.h"
using namespace std;






class Fahrausnahme: exception
{
public:
	Fahrausnahme(Fahrzeug& Fahrzeug, Weg& Weg,string S1, string S2);
	virtual void vBearbeiten() =0;				//rein virtuelle Funktion werden durch das = 0 gekennzeichnet, dienen nur zur Spezifikation
protected:
	Fahrzeug& r_Fahrzeug;
	Weg& r_Weg;
	string  St1;
	string St2;


};

#endif /* SRC_FAHRAUSNAHME_H_ */
