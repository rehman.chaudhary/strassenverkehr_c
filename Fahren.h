/*
 * Fahren.h
 *
 *  Created on: 30.11.2023
 *      Author: rehmanchaudhary
 */

#ifndef SRC_FAHREN_H_
#define SRC_FAHREN_H_
#include "Verhalten.h"


class Fahren : public Verhalten
{
public:
	double dStrecke(Fahrzeug & aFzg, double dZeitIntervall) override ;
	Fahren(Weg& Weg);
};

#endif /* SRC_FAHREN_H_ */
