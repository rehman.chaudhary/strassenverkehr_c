/*
 * Simulationsobjekt.cpp
 *
 *  Created on: 19.11.2023
 *      Author: rehmanchaudhary
 */



#include "Simulationsobjekt.h"


int  Simulationsobjekt::p_iMaxID = 0;

/**
 * @brief Standardkonstruktor
 */
Simulationsobjekt::Simulationsobjekt()
{
	p_iMaxID++;
	p_iID = p_iMaxID;
	//cout<<"Standardkonstruktor"<<endl;
}

/**
 * @brief Konstruktor, der einen neues Fahrzeug  mit allen Werten erstellt
 * @param sName: Name des Autos
 */
Simulationsobjekt::Simulationsobjekt(string sName): p_sName(sName)
{

	p_iMaxID++;
	p_iID = p_iMaxID;
	//cout<<"Name: "<<p_sName<<" ID: "<< p_iID<<endl;
}


void Simulationsobjekt::vKopf()
{
	cout<<resetiosflags(ios::right) << setiosflags(ios::left)<<setw(5)<<left<<"ID"<<setw(20)<<right<<"Name"<<setw(25)<<endl;
	cout<<"-----------------------------------------------------------------------------------------------------------------------------------------"<<endl;
}


/**
 * @brief Erstellt die ausgegebene Tabelle
 */
void Simulationsobjekt::vAusgeben() const
{
	cout<<setiosflags(ios::left)<<setw(5)<<left<<p_iID<<setw(15)<<right<<p_sName;
}




ostream &Simulationsobjekt::oAusgeben(ostream &out) const
{
	out<<setiosflags(ios::left)<<setw(5)<<left<<p_iID<<setw(20)<<left<<p_sName;
	return out;
}


ostream& operator <<(ostream& out, const Simulationsobjekt& Simulationsobjekt)
{
	 Simulationsobjekt.oAusgeben(out);
	 return out;
}


string Simulationsobjekt::getName() const
{
	return p_sName;
}

/**
 * @brief Destruktor: löscht die erstellten Instanzen
 */
Simulationsobjekt::~Simulationsobjekt()
{}

