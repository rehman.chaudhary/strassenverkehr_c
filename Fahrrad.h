/*
 * Fahrrad.h
 *
 *  Created on: 02.11.2023
 *      Author: rehmanchaudhary
 */

#ifndef FAHRRAD_H_
#define FAHRRAD_H_
#include "Fahrzeug.h"




class Fahrrad : public Fahrzeug
{
public:
	Fahrrad();
	Fahrrad(string sName, double dGeschwindigkeit);
	virtual double dGeschwindigkeit() const ;
    void vZeichnen(const Weg& Weg) const override;

	virtual ~Fahrrad();


};


#endif /* FAHRRAD_H_ */
