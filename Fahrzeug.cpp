/*
 * Fahrzeug.cpp
 *
 *  Created on: 10.10.2023
 *      Author: rehmanchaudhary
 */

#include "Fahrzeug.h"
#include "Fahrrad.h"
#include "Verhalten.h"
#include "Fahren.h"
#include "Parken.h"






/**
 * @brief Standardkonstruktor
 */
Fahrzeug::Fahrzeug(): Simulationsobjekt()
{
}

/**
 * @brief Konstruktor, der einen neues Fahrzeug  mit allen Werten erstellt
 * @param sName: Name des Autos
 */
Fahrzeug::Fahrzeug(const string sName) : Simulationsobjekt(sName)
{
	p_iMaxID++;
	p_iID = p_iMaxID;
    //cout<<"Das Fahrzeug "<<p_sName<<" mit der ID Nummer "<<p_iID<<" wurde erstellt."<<endl;

}

/**
 * @brief Konstruktor mit Übergabeparameter
 * @param sName : Name des Autos
 * @param i_MaxGeschwindigkeit: maximale Geschwindigkeit des Fahrzeugs
 */
Fahrzeug::Fahrzeug(const string sName, const double dMaxGeschwindigkeit): p_sName(sName), p_dMaxGeschwindigkeit((dMaxGeschwindigkeit)>=0 ? dMaxGeschwindigkeit:0)
{

	p_dMaxGeschwindigkeit = dMaxGeschwindigkeit;
	p_iMaxID++;
	p_iID = p_iMaxID;
	//cout<<"Das Fahrzeug "<<p_sName<<" mit der ID Nummer "<<p_iID<<" wurde erstellt. Es  fährt mit der max. Geschwindigkeit "<< p_dMaxGeschwindigkeit <<"."<<endl;
}



/**
 * @brief Destruktor: löscht die erstellten Instanzen
 */
Fahrzeug::~Fahrzeug()
{
	//cout<<endl;
	//cout<<"Der Desktruktor wurde aufgerufen und hat die Instanz mit der ID Nummer "<<p_iID<< " gelöscht."<<endl;
}



/**
 * @brief Erstellt den Kopf der Tabelle
 */

void Fahrzeug::vKopf()
{
	cout<<resetiosflags(ios::right) << setiosflags(ios::left)<<setw(5)<<left<<"ID"<<setw(20)<<right<<"Name"<<setw(25)<<right<<"akt. Geschwindigkeit"<<setw(30)<<right<<"Gesamtstrecke" <<setw(20)<<right<<"Verbrauch"<<setw(30)<<right<<"Tankinhalt"<<endl;
	cout<<"-----------------------------------------------------------------------------------------------------------------------------------------"<<endl;
}



/**
 * @brief Erstellt die ausgegebene Tabelle
 */
void Fahrzeug::vAusgeben() const
{
	cout<<setiosflags(ios::left)<<setw(5)<<left<<p_iID<<setw(20)<<right<<p_sName<<setw(25)<<setprecision(2)<<fixed<<right<<dGeschwindigkeit()<<setw(25)<<setprecision(2)<<fixed<<right<<p_dGesamtStrecke;
}





/**
 *@brief Funktion ist für die nicht tankabren Fahrzeuge implementiert
 *von hier aus können alle abgeleitetetn Klassen auf diee Funktion zugreifen
 *@return 0: gibt einfach 0 aus
*/
double Fahrzeug::dTanken(double dMenge)
{
	return 0;
}


double Fahrzeug::getWegLaenge()const
{
	if(p_pVerhalten)
	{
		return p_pVerhalten->getWegLaenge();
	}
	return 0.0;
}


void Fahrzeug::vSimulieren()
{
if (dGlobaleZeit > p_dZeit)
{
	double dFahrzeit = dGlobaleZeit - p_dZeit; // dFahrzeit entspricht Zeittakt
	double dTeilstrecke = p_pVerhalten.get()->dStrecke(*this, dFahrzeit);
	p_dAbschnittStrecke += dTeilstrecke; /* Aktuelle Strecke auf dem Weg wird aktualisiert */
	p_dGesamtStrecke += dTeilstrecke; /* Gesamtstrecke wird aktualisiert. */
	p_dZeit += dFahrzeit; /*aktualisiern nach dem Takt. */
	p_dGesamtZeit += dFahrzeit; /* gesamtzeit wird aktualisiert. */

}
if(p_dGesamtStrecke>=this->getWegLaenge())
		{
			cout<<"DAS FAHRZEUG "<<p_sName<<" HAT DAS ENDE DES WEGES ERREICHT."<<endl;
		}
else
	{
	 cout<<"Das folgende Fahrzeug befindet sich auf dem Weg: "<<p_sName<<endl;
	}
}


/**
 *@brief gibt die Geschwindigkeit der Fahrzeuge aus
 *@return gibt die aktuelle Geschwindigkeit aus
*/
double Fahrzeug::dGeschwindigkeit() const
{
	return p_dMaxGeschwindigkeit;
}

string Fahrzeug::getName() const
{
	return p_sName;
}



/**
 *@brief
*/
void Fahrzeug::vNeueStrecke(Weg &Weg)
{
	this->p_pVerhalten = make_shared<Fahren>(Weg);

}

void Fahrzeug::vNeueStrecke(Weg& Weg,double dStartzeit)
{
	this->p_pVerhalten= make_shared<Parken>(Weg,dStartzeit);

}


double Fahrzeug::dgetAbschnittStrecke() const
{
	return p_dAbschnittStrecke;
}

void Fahrzeug::vZeichnen(const Weg& Weg) const
{}


/**
 *@return Zeit : gibt die p_dZeit zurück
*/
double Fahrzeug::getZeit() const
{
	return p_dZeit;
}


/**
 *@brief Überladene Ausgeben-Funktion
 *@return out : gibt die gewünschte Ausgabe
*/
ostream &Fahrzeug::oAusgeben(ostream &out)const
{
	out<<setiosflags(ios::left)<<setw(5)<<left<<p_iID<<setw(20)<<right<<p_sName<<setw(25)<<setprecision(2)<<fixed<<right<<dGeschwindigkeit()<<setw(25)<<setprecision(2)<<fixed<<right<<p_dGesamtStrecke;
	return out;
}




/**
 *@brief Überladenr Opterator <
 *@return true --> falls Gesamtstrecke kleiner als das Verrgleichsobjekt
 *@return false--> falls Gesamtstrecke grösser als das Vergleichsobjekt
*/
bool Fahrzeug::operator<(const Fahrzeug& fahrzeug) const
{
	if (p_dGesamtStrecke < fahrzeug.p_dGesamtStrecke)
	{
		return true;
	}
	else
	{
		return false;
	}
}


/**
 *@brief Überladenr Opterator =
*/
void Fahrzeug::operator = (const Fahrzeug& fahrzeug)
{
	p_sName = fahrzeug.p_sName ;
	p_dMaxGeschwindigkeit = fahrzeug.p_dMaxGeschwindigkeit;
}





/**
 *@brief Überladenr Opterator ==
 *@return true --> falls die IDs der der Objekte gleich sind
 *@return false--> falls die IDs der Objekte nicht gleich sind
*/
bool Fahrzeug::operator==(const Fahrzeug& fahrzeug) const
{
	if(p_iID == fahrzeug.p_iID)
	{
		return true;
	}
	else
		{
			return false;
		}
}
