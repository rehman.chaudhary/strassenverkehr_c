/*
 * Losfahren.h
 *
 *  Created on: 02.12.2023
 *      Author: rehmanchaudhary
 */

#ifndef SRC_LOSFAHREN_H_
#define SRC_LOSFAHREN_H_
#include "Fahrausnahme.h"



class Losfahren: public Fahrausnahme
{
public:
	Losfahren(Fahrzeug& Fahrzeug,Weg& Weg);
    void vBearbeiten() override;
};

#endif /* SRC_LOSFAHREN_H_ */
