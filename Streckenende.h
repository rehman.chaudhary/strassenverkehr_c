/*
 * Streckenende.h
 *
 *  Created on: 02.12.2023
 *      Author: rehmanchaudhary
 */

#ifndef SRC_STRECKENENDE_H_
#define SRC_STRECKENENDE_H_
#pragma once
#include "Fahrausnahme.h"



class Streckenende : public Fahrausnahme
{
public:
	Streckenende(Fahrzeug& Fahrzeug, Weg& Weg);
	void vBearbeiten() override;

};

#endif /* SRC_STRECKENENDE_H_ */
