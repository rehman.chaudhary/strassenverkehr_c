/*
 * Verhalten.cpp
 *
 *  Created on: 29.11.2023
 *      Author: rehmanchaudhary
 */

#include "Verhalten.h"
#include "Weg.h"
#include "Fahrzeug.h"





Verhalten::Verhalten(Weg &Weg):p_pWeg(Weg)
{}



double Verhalten::getWegLaenge() const
{
	return p_pWeg.getLaenge();
}

Weg& Verhalten::getWeg()
{
	return p_pWeg;
}

Verhalten::~Verhalten()
{
	// TODO Auto-generated destructor stub
}

