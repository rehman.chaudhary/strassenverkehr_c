/*
 * Parken.cpp
 *
 *  Created on: 30.11.2023
 *      Author: rehmanchaudhary
 */
#include <iostream>
#include "Parken.h"
#include "Verhalten.h"
#include "Fahrzeug.h"
#include "Losfahren.h"



extern double dGlobaleZeit;
using namespace std;


Parken::Parken(Weg& Weg, double StartZeit):Verhalten(Weg),dStartZeit(StartZeit)
{}

double Parken::dStrecke(Fahrzeug& aFzg, double dZeitIntervall)
{
	if(dStartZeit - (aFzg.getZeit() + dZeitIntervall) <= 1e-6)								//falls Startuzeit kleiner ist als globaleZeit--> ganz normale Ausführung
	{
		throw Losfahren(aFzg, p_pWeg);
	}
	else
	{											//Startzeit zeigt, ab wann welchem Zeitpunkt das Auto losfährt
		return 0.0;
	}
}



