/*
 * Fahren.cpp
 *
 *  Created on: 30.11.2023
 *      Author: rehmanchaudhary
 */

#include "Fahren.h"
#include "Weg.h"
#include "Fahrzeug.h"
#include "Streckenende.h"


#include <cmath>

Fahren::Fahren(Weg& Weg):Verhalten(Weg)
{}

double Fahren::dStrecke(Fahrzeug& aFzg, double dZeitIntervall)
{
	// dStrecke fur fahrende Fahrzeuge
	double dFahrstrecke;
	dFahrstrecke = aFzg.dGeschwindigkeit() * dZeitIntervall;
	if (aFzg.dgetAbschnittStrecke() + dFahrstrecke - p_pWeg.getLaenge() < 1e-6)
	{
		// Prueft, ob der Weg genug Strecke fuer die theoretische Fahrstrecke hat
		return dFahrstrecke;
	}

	else if (fabs(p_pWeg.getLaenge() - aFzg.dgetAbschnittStrecke()) < 1e-6) // fabs = die restliche Strecke eines Weges
	{
		// Falls das Fahrzeug am Ende des Wegs kommt
		// Exception, Fahrausnahmeelement von Streckenende erzeugen und werfen
		throw Streckenende(aFzg, p_pWeg);

	}

	else
	{
		// Falls die theoretische Fahrstrecke laenger als den Rest(d.h naechste Mal wird das Fahrzeug am Ende des Wegs kommt)
		// Rest zurueckliefern
		return (p_pWeg.getLaenge() - aFzg.dgetAbschnittStrecke());
	}
}
