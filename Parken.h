/*
 * Parken.h
 *
 *  Created on: 30.11.2023
 *      Author: rehmanchaudhary
 */

#ifndef SRC_PARKEN_H_
#define SRC_PARKEN_H_
#include "Verhalten.h"

class Parken: public Verhalten
{
public:
	Parken(Weg& Weg, double StartZeit);
	virtual double dStrecke(Fahrzeug &aFgz, double dZeitIntervall) override;


private:
	double dStartZeit;
};

#endif /* SRC_PARKEN_H_ */
